function updatePrice() {
  
  let s = document.getElementsByName("ticketType");
  let select = s[0];
  let price = 0;
  let prices = getPrices();
  let priceIndex = parseInt(select.value) - 1;
  if (priceIndex >= 0) {
    price = prices.ticketType[priceIndex];
  }
  
  let radioDiv = document.getElementById("radios");
  
  let checkDiv = document.getElementById("checkboxes");
  
  if(select.value == 1){radioDiv.style.display="none";checkDiv.style.display="none";}
  else if(select.value==2){ radioDiv.style.display="block";checkDiv.style.display="none";}
  else if(select.value==3){ radioDiv.style.display="none";checkDiv.style.display="block"}
  
  let radios = document.getElementsByName("ticketOp");
  radios.forEach(function(radio) {
    if (radio.checked) {
      let optionPrice = prices.ticketOp[radio.value];
      if (optionPrice !== undefined) {
        price += optionPrice;
      }
    }
  });
   
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    if (checkbox.checked) {
      let propPrice = prices.prodProperties[checkbox.name];
      if (propPrice !== undefined) {
        price += propPrice;
      }
    }
  });
  
  let val = document.getElementById("amount").value;
  let ticket_price = document.getElementById("ticket_price");
  if(!(/^[1-9][0-9]*$/).test(val)){ticket_price.innerHTML="Введено неверное количество билетов";return;}
  val=parseInt(val);
  ticket_price.innerHTML = "Стоимость "+val+" билетов равна " + price*val + " рублей";
}
function getPrices() {
  return {
    ticketType: [690, 2000, 7000],
    ticketOp: {
	  option1: 200,
      option2: 400,
      option3: 1000,
      option4: 300,
    },
    prodProperties: {
      prop1: 2200,
      prop2: 1000,
      prop3: 500,
    }
  };
}

window.addEventListener('DOMContentLoaded', function (event) {
  
  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = "none";
  
 
  let s = document.getElementsByName("ticketType");
  let select = s[0];
  
  select.addEventListener("change", function(event) {
    updatePrice();
  });
  
  
  let radios = document.getElementsByName("ticketOp");
  radios.forEach(function(radio) {
    radio.addEventListener("change", function(event) {
      updatePrice();
    });
  });

   
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    checkbox.addEventListener("change", function(event) {
      updatePrice();
    });
  });
  
  let val = document.getElementById("amount");
  val.addEventListener("change", updatePrice);

  updatePrice();
});
